---
layout: markdown_page
title: "Engineering Career Development"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Individual Contribution vs. Management

Most important is the fork between purely technical work and managing teams. It's important that Engineering Managers self-select into that track and don't feel pressured. We believe that management is a craft like any other and requires dedication. We also believe that everyone deserves a manager that is passionate about their craft.

Once someone reaches a Senior-level role, and wants to progress, they will need to decide whether they want to remain purely technical or pursue managing technical teams. Their manager can provide opportunities to try tasks from both tracks if they wish. Staff-level roles and Engineering Manager roles are equivalent in terms of base compensation and prestige.


## Roles

<table style="text-align:center; border-spacing:5px; border-collapse:separate; font-size:14px;">
  <tr>
    <td colspan="6" style="border:0;">&nbsp;</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/developer#staff-developer">Staff Developer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/developer#distinguished-developer">Distinguished Developer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/developer#engineering-fellow">Engineering Fellow</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/chief-technology-officer/">Chief Technology Officer</a></td>
  </tr>
  <tr>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/developer#junior-developer">Junior Developer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/developer#intermediate-developer">Intermediate Developer</a></td>
  <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/developer#senior-developer">Senior Developer</a></td>
    <td style="border:0; vertical-align:middle;">↗</td>
  </tr>
  <tr>
    <td style="border:0; vertical-align:middle;">↘</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/engineering-management#engineering-manager">Engineering Manager</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/engineering-management#director-of-engineering">Director of Engineering</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/engineering-management#vp-of-engineering">VP of Engineering</a></td>
  </tr>
  <tr>
    <td style="border:0;">&nbsp;</td>
  </tr>
  <tr>
    <td style="border:0;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6" style="border:0;">&nbsp;</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/security#staff-security-engineer">Staff Security Engineer</a></td>
  </tr>
  <tr>
    <td rowspan="2" colspan="2" style="border:0;"></td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/security#security-engineer">Security Engineer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/security#senior-security-engineer">Senior Security Engineer</a></td>
    <td style="border:0; vertical-align:middle;">↗</td>
  </tr>
  <tr>
    <td style="border:0; vertical-align:middle;">↘</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/security-management#security-engineering-manager">Security Engineering Manager</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/security-management#director-of-security">Director of Security</a></td>
  </tr>
  <tr>
    <td style="border:0;">&nbsp;</td>
  </tr>
  <tr>
    <td style="border:0;">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="6" style="border:0;">&nbsp;</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;">Staff Automation Engineer</td>
  </tr>
  <tr>
    <td rowspan="2" colspan="2" style="border:0;"></td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/automation-engineer">Automation Engineer</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;">Senior Automation Engineer</td>
    <td style="border:0; vertical-align:middle;">↗</td>
  </tr>
  <tr>
    <td style="border:0; vertical-align:middle;">↘</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;"><a href="/jobs/manager-test-automation">Manager, Test Automation</a></td>
    <td rowspan="2" style="border:0; vertical-align:middle;">→</td>
    <td rowspan="2" style="border:0; vertical-align:middle; background:#f5f5f5;">Director of Quality</td>
  </tr>
  <tr>
    <td style="border:0;">&nbsp;</td>
  </tr>
</table>


## Senior Developers

Note that we have a specific section for Senior Developer because it's an important step in the technical development for every engineer. But "Senior" can optionally be applied to any role here indicating superior performance. However, it's not required to pass through "senior" for roles other than Developer. Also note that we occasionally hire Junior Developers if they have demonstrated both aptitude and our values and we believe they will quickly develop into a Developer.

Senior developers typically receive fewer trivial comments on their merge requests. Attention to detail is very important to us. They also receive fewer _major_ comments because they understand the application architecture and select from proven patterns. We also expect senior developers to come up with simpler solutions to complex problems. Managing complexity is key to their work.


## Promotion

We strive to set the clearest possible expectations with regard to performance and promotions. Nevertheless, some aspects are qualitative. Examples of attributes that are hard to quantify are communication skills, mentorship ability, accountability, and positive contributions to company culture and the sense of psychological safety on teams. For these attributes we primarily rely on the experience of our managers and the performance review process (especially peer reviews). It's our belief that while a manager provides feedback and opportunities for improvement or development, that it's actually the team that elevates individuals.

#### Transfer Options

The following table outlines the lateral transfer options at any level of the role. Experience factor might differ per individual to determine leveling for each of the positions listed.

| Starting Role                        | Lateral Options                   |
|--------------------------------------|-----------------------------------|
| Frontend Engineer                    | UX Designer                       |
| UX Designer                          | Frontend Engineer                 |
| Developer                            | Production Engineer               |
| Production Engineer                  | Developer                         |
| Developer                            | Support Engineer                  |
| Support Engineer                     | Developer                         |
| Support Engineer                     | Solutions Architect               |
| Support Engineer                     | Implementation Specialist         |
| Automation Engineer                  | Developer                         |
| Developer                            | Automation Engineer               |

Specific backend teams can also be looked at for a lateral transfer. Those teams include Build, Platform, CI/CD, Geo, Prometheus, Gitaly, etc.
