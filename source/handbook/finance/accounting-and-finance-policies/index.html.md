---
layout: markdown_page
title: "Accounting and Finance Policies"
---

## Accounting and Finance Policies

This page contains GitLab's accounting and finance policies.

- [Capital Assets](/handbook/finance/capital-assets-policy/)
- [Procure to Pay](/handbook/finance/procure-to-pay/)