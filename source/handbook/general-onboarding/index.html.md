---
layout: markdown_page
title: "GitLab Onboarding"
---

All onboarding steps are being moved into the [onboarding issue template](https://gitlab.com/gitlab-com/peopleops/edit/master/.gitlab/issue_templates/onboarding.md) in the [People Ops project](https://gitlab.com/gitlab-com/peopleops).

## Other pages

* [Sales onboarding](/handbook/sales-onboarding)
* [Developer onboarding](/handbook/developer-onboarding)
* [UX Designer onboarding](/handbook/uxdesigner-onboarding)
* [UX Researcher onboarding](/handbook/uxresearcher-onboarding)
* [Designer (marketing) onboarding](/handbook/designer-onboarding)
* [Support Engineer onboarding](/handbook/support/onboarding/)
* [Offboarding](/handbook/offboarding)
* [Glossary](/handbook/glossary/)
* [Onboarding Processes](/handbook/general-onboarding/onboarding-processes)
