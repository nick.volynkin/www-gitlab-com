---
layout: markdown_page
title: "Online Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Other Important Pages
* [Google Analytics Crash Course](/handbook/marketing/lead-generation/online-marketing/ga-training)

----

Welcome to the Online Marketing Handbook.

## Requesting Marketing Campaigns

If you would like to request a paid marketing campaign to support your event, content marketing, or webcast, please create an issue in the marketing repo and use the `Social-Ads` template to create an issue. Assign the issue to `@courtlandia`.

## URL Tagging

All marketing campaigns that are promoted on external sites and through email should use URL tagging to increase the data cleanliness in Google Analytics. For a primer course, please see the [training video](https://drive.google.com/a/gitlab.com/file/d/0B1_ZzeTfG3XYNWVqOC11NWpKWjA/view?usp=sharing). This explains the why and how of URL tagging.

Our internal URL tagging tool can be accessed on Google Docs under the name [Google Analytics Campaign Tagging Tool](https://docs.google.com/a/gitlab.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit?usp=sharing).

## Weekly Website Health Check

Every Friday a website health check should be performed. This check is meant to ensure that there are no issues with the website that could cause issues with traffic to the site. These are the following things to check to ensure the health of the site:

* [Google Webmaster Tools](https://www.google.com/webmasters/tools/)
* [Bing Webmaster Tools](http://www.bing.com/toolbox/webmaster)
* [Google Analytics](https://analytics.google.com/analytics/web/)

Issues to look for in each of these tools:

* **Google Webmaster Tools**: Check the dashboard and messages for any important notifications regarding the website. Also check under `Search Traffic` > `Manual Actions` for any URLs that have been identified as spam or harmful content.
* **Bing Webmaster Tools**: Check all the links under `Security`.
* **Google Analytics**: Compare organic site traffic from the most previous week compared to the previous week and look for any large fluctuations.

## Reporting

Information about reporting done by the Online Marketing team and across the Marketing functional group can be found in the [Business Operations - Reporting](/handbook/business-ops/reporting/) section. 

## Online Marketing Tools

We use a variety of tools to support the other team within the company. Details about the tech stack, who has access and the system admins is found in the [Business Operations handbook](/handbook/business-ops/#tech-stack) section.

## Adding Redirects To GitLab's Website

Occasionally we will need to change URL structures of the website and we want to make sure that people trying to view those pages can find the information they need. Because working with the NGINX setup is a technical process, so we rely on the Production team to help with these requests. 

Make the request in the `#production` channel on Slack. Provide the following:
* Old URL that needs to be redirected
* New URL where users should now be sent
