---
layout: job_page
title: "Executive Assistant"
---

## Responsibilities

GitLab is hiring an executive assistant to support our leadership teams in EST - PST timezones. We’re searching for a self-driven, collaborative,
and agile team member who is experienced in managing multiple priorities, juggling various responsibilities, and anticipating
executives’ needs. Executive assistants at GitLab toggle seamlessly between various systems including Gmail, Slack, Expensify, and
GitLab to accomplish an array of tasks, while staying focused on prioritization and escalating urgent issues. The perfect candidate will be
exceptionally organized personally and enjoys organizing for others, has a deep love of logistics and thrives in a dynamic start-up environment.
We're looking for a talented communicator and an expert planner to ensure GitLab continues to run smoothly.


## Requirements
* Minimum of 2 years of executive administration supporting more than one executive.
* Must be able to support team members in PST and match the CEO's working schedule.
* Successful history of managing the calendars, expenses, and travel of multiple executives.
* Experience predicting, prioritizing, and assisting an executive’s workload.
* Strong knowledge of Google Apps (Gmail, Docs, Spreadsheets, Presentations). Slack is a plus but not necessary.
* Exceptional communication and interpersonal skills and ability to interact autonomously with internal and external partners.
* Must have a proven track record of discretion with sensitive company and personal information.
* Excellent computer skills, self starter in picking up new and complex systems.
* Demonstrated ability to adopt technical tools quickly (i.e. terminal, text editor).
* Possess an ability to multi-task and prioritize in a dynamic environment.
* Superior attention to detail.
* Event coordination and creative event planning experience.
* Excellent written and verbal English communication skills.
* Experience in a start-up environment preferred.
* Experience working remotely preferred.
- Successful completion of a [background check](/handbook/people-operations/#background-checks).


## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
