---
layout: job_page
title: "Federal Solutions Architect"
---

Solution Architects are the trusted advisors to GitLab prospects and clients, showing how the GitLab solutions address clients business requirements. Solution Architects are responsible for actively driving and managing the technology evaluation and validation stages of the sales process. Solution Architects are the product advocates for GitLab’s Enterprise Edition, serving as a trusted advisor to the client, focusing on the the technical solution while also understanding the business challenges the customer is trying to overcome.

The Solution Architect for Customer Success Initiatives provides the opportunity to help drive value and change in the world of software development for one of the fastest growing platforms.  By applying your solution selling and architecture experience from idea to production, you will support and enable successful adoption of the GitLab platform.  You will be working directly with our top enterprise customers.  You will be working collaboratively with our sales, engineering, product management and marketing organizations.  This role provides technical guidance and support through the entire sales cycle.  You will have the opportunity to help shape and execute a strategy to build mindshare and broad use of the GitLab platform within enterprise customers becoming the trusted advisor. The ideal candidate must be self-motivated with a proven track record in software/technology sales or consulting. The ability to connect technology with measurable business value is critical to a solutions architect. You should also have a demonstrated ability to think strategically about business, products, and technical challenges.


## Responsibilities

* In partnership with the sales team, formulate and execute a sales strategy to exceed revenue objectives through the adoption of GitLab
* Educate customers of all sizes on the value proposition of Gitlab, and participate in all levels of discussions throughout the organization to ensure our solution is setup for successful deployment
* Work on­site with strategic, enterprise­ class customers, delivering solutions architecture consulting, technical guidance, knowledge transfer and establish “trusted advisor status”
* Capture and share best-practice knowledge amongst the GitLab community
* Author or otherwise contribute to GitLab customer-facing publications such as whitepapers, blogs, GitLab Handbook
* Build deep relationships with senior technical individuals within customers to enable them to be GitLab advocates
* Serve as the customer advocate to other GitLab teams including Product Development, Sales, and Marketing
Present GitLab platform strategy, concepts, and roadmap to technical leaders with the customer’s organization
* Participating in conferences and trade shows and interacting with government customers in attendance
* Contribute to the creation of case studies, white papers, and media articles for government customers and/or partner
* Consistently provide world-class customer service during pre-sales, implementation, and post-sales activities
* Manage all technical aspects of the sales cycle: creating high quality professional presentations, custom demos, proof of concepts, deliver technical deep dive sessions & workshops, differentiate GitLab from competition, answering RFI, RFPs, etc.

## Preferred Qualifications

* Presentation skills with a high degree of comfort speaking with executives, IT Management, and developers.
* Strong written communication skills
* High level of comfort communicating effectively across internal and external organizations
* Ideal candidates will preferably have 7 plus years IT industry or IT technical sales related to experience with a proven track record of solution sales expertise
* Significant experience with executive presence and a proven ability to lead and facilitate executive meetings and workshops.
* Security Clearance preferred, but not required
* Knowledge and at least 4 years of experience with Federal customers
* Ability to travel up to 50%

## Technical Requirements

* Deep knowledge of software development lifecycle and development pipeline (idea to production)
* Understanding of continuous integration, continuous deployment, chatOps, and cloud native
* Experience with waterfall, Agile (SCRUM, Kanban, etc) and able to discuss the value of different software development processes
* Understand mono-repo and distributed-repo approaches
* Above average knowledge of Unix and Unix based Operating Systems
* Installation and operation of Linux operating systems and hardware investigation / manipulation commands
  * BASH / Shell scripting including systems and init.d startup scripts
  * Package management (RPM, etc. to add/remove/list packages)
  * Understanding of system log files / logging infrastructure
  * Strong desire to learn new things
* Experience with container systems preferred:
  * Kubernetes / Docker
  * Installation / configuration
  * Container registries
* Experience with any of the following tools / software packages preferred:
  * Ruby on Rails Applications
  * Git, BitBucket/Stash, GitHub, Perforce, SVN
  * Jira, Jenkins
* B.Sc. in Computer Science or equivalent experience
- Successful completion of a [background check](/handbook/people-operations/#background-checks).


## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Selected candidates will be invited to schedule a 45min [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule interviews with Director of Customer success
* The strongest candidates may be asked to schedule additional 45 minute interviews with an Account Executive, a Regional Sales Director, and our CRO
* Finally, candidates will interview with our CEO

**Note on Compensation**

As you use the compensation calculator below, please bear in mind that for this
role the calculated compensation represents your On Target Earnings (OTE).  You
will typically get 80% as base, and 20% based on meeting the
global sales goal of incremental ACV. Also see the [Sales Compensation Plan](https://about.gitlab.com/handbook/finance/sales-comp-plan/).

## Apply

Please note that if we are actively hiring for a position, you will see it listed on our [jobs page](https://about.gitlab.com/jobs/), where all of our current openings are advertised. To apply, please click on the name of the role you are interested in, which will take you to our applicant tracking system (ATS), [Lever](https://www.lever.co/).

Avoid the [confidence gap](https://www.theatlantic.com/magazine/archive/2014/05/the-confidence-gap/359815/
); you do not have to match all the listed requirements exactly to apply. Our hiring process is described in more detail in our [hiring handbook](https://about.gitlab.com/handbook/hiring/).

## About GitLab

GitLab Inc. is a company based on the GitLab open-source project. GitLab is a community project to which over 1,000 people worldwide have contributed. We are an active participant in this community, trying to serve its needs and lead by example. We have one [vision](https://about.gitlab.com/strategy/): everyone can contribute to all digital content, and our mission is to change all creative work from read-only to read-write so that everyone can contribute.

We [value](https://about.gitlab.com/handbook/values/) results, transparency, sharing, freedom, efficiency, frugality, collaboration, directness, kindness, diversity, boring solutions, and quirkiness. If these values match your personality, work ethic, and personal goals, we encourage you to visit our [primer](https://about.gitlab.com/primer/) to learn more. Open source is our culture, our way of life, our story, and what makes us truly unique.

Top 10 reasons to work for GitLab:

1. Work with helpful, kind, motivated, and talented people.
1. Work remote so you have no commute and are free to travel and move.
1. Have flexible work hours so you are there for other people and free to plan the day how you like.
1. Everyone works remote, but you don't feel remote. We don't have a head office, so you're not in a satellite office.
1. Work on open source software so you can interact with a large community and can show your work.
1. Work on a product you use every day: we drink our own wine.
1. Work on a product used by lots of people that care about what you do.
1. As a company we contribute more than we take, most of our work is released as the open source GitLab CE.
1. Focused on results, not on long hours, so that you can have a life and don't burn out.
1. Open internal processes: know what you're getting in to and be assured we're thoughtful and effective.

See [our culture page](https://about.gitlab.com/culture) for more!

Work remotely from anywhere in the world. Curious to see what that looks like? Check out our [remote manifesto](https://about.gitlab.com/2015/04/08/the-remote-manifesto/).
