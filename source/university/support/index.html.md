---
layout: markdown_page
title: University | Support Path
---

**This page has been moved to the new [GitLab University support path](https://docs.gitlab.com/ce/university/support/) page**
